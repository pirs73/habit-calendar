import React, { useState, useEffect } from 'react';
import { isSameDay, getDaysInMonth, getDate } from 'date-fns';
import { enUS } from 'date-fns/locale';
import { utcToZonedTime } from 'date-fns-tz';
import { instanceOf, func, array } from 'prop-types';
import { connect } from 'react-redux';
import { Calendar, ChartYear } from '../../components';
import { updateYearRate } from '../../redux/graph/graph.actions';
import { updateYearStreak } from '../../redux/calendar/calendar.actions';
import { TIME_ZONE } from '../../constants';
import { getYearMonth, namesMonth } from '../../utils';
import './Home.css';

const _Home = ({
  selectedMonth,
  updateYearRate,
  updateYearStreak,
  yearStreakStat,
}) => {
  const [selectedDates, setSelectedDates] = useState([]);
  const [selectedDateByMonth, setSelectedDateByMonth] = useState([]);
  const [successRate, setSuccessRate] = useState(0);
  const [monthStreak, setMonthStreak] = useState(0);
  const [checked, setChecked] = useState(true);
  const [bestStreak, setBestStreak] = useState(0);

  const modifiers = {
    selected: (date) =>
      selectedDates.some((selectedDate) => isSameDay(selectedDate, date)),
  };

  const handleDayClick = (date) => {
    const existingDate = selectedDates.includes(Date.parse(date));
    if (existingDate) {
      const filtredDates = selectedDates.filter((d) => d !== Date.parse(date));
      setSelectedDates(filtredDates);
    } else {
      setSelectedDates([...selectedDates, Date.parse(date)]);
    }
  };

  const compareDate = (month, date) => {
    const compMonth1 = utcToZonedTime(month, TIME_ZONE).getMonth();
    const compMonth2 = new Date(date).getMonth();
    const compYear1 = utcToZonedTime(month, TIME_ZONE).getFullYear();
    const compYear2 = new Date(date).getFullYear();

    if (compMonth1 === compMonth2 && compYear1 === compYear2) {
      return true;
    } else {
      return false;
    }
  };

  useEffect(() => {
    const selectedMonthData = selectedDates.filter((date) =>
      compareDate(selectedMonth, date),
    );

    setSelectedDateByMonth(selectedMonthData);
  }, [selectedMonth, selectedDates]);

  useEffect(() => {
    const rate = Math.round(
      (selectedDateByMonth.length / getDaysInMonth(selectedMonth)) * 100,
    );
    setSuccessRate(rate);

    const yearMonthRate = {};

    if (selectedMonth) {
      const { monthRate, yearRate } = getYearMonth(selectedMonth);
      yearMonthRate.month = monthRate;
      yearMonthRate.year = yearRate;
    }

    yearMonthRate.rate = rate;

    const newData = namesMonth(yearMonthRate);

    updateYearRate(newData);
  }, [selectedDateByMonth, selectedMonth, updateYearRate]);

  useEffect(() => {
    const sumTo = (n) => {
      return (n * (n + 1)) / 2;
    };

    const dayMonthArr = selectedDateByMonth.map((date) => {
      return getDate(new Date(date));
    });

    const sortArr = dayMonthArr.sort((a, b) => a - b);

    const tmpArr = [];
    let result = 0;

    for (let i = 0; i < sortArr.length; ++i) {
      result = tmpArr.reduce(function (sum, current) {
        return sum + current;
      }, 0);
      if (sortArr[0] === 1 && result === sumTo(tmpArr.length)) {
        tmpArr.push(sortArr[i]);
      }
    }

    for (let i = 0; i < sortArr.length; ++i) {
      result = tmpArr.reduce(function (sum, current) {
        return sum + current;
      }, 0);
      if (result !== sumTo(tmpArr.length)) {
        tmpArr.pop();
      }
    }

    setMonthStreak(tmpArr.length);

    const yearStreak = {};

    if (selectedMonth) {
      const { monthRate, yearRate } = getYearMonth(selectedMonth);
      yearStreak.month = monthRate;
      yearStreak.year = yearRate;
    }

    yearStreak.streak = tmpArr.length;

    updateYearStreak(yearStreak);
  }, [selectedDateByMonth, selectedMonth, updateYearStreak]);

  useEffect(() => {
    const statsByYear = (month, stats) => {
      const { yearRate } = getYearMonth(month);
      return stats.filter((stat) => stat.year === yearRate);
    };

    let tmpArr = [];
    tmpArr = [...yearStreakStat];

    if (selectedMonth) {
      const data = statsByYear(selectedMonth, tmpArr);

      const streakByYear = data.map((d) => d.streak);
      setBestStreak(Math.max.apply(null, streakByYear));
    }
  }, [yearStreakStat, selectedMonth]);

  return (
    <div className="home">
      <h2 className="home__title">Multitasking</h2>
      <div className="container">
        <div className="left-block"></div>
        <div className="right-block">
          <div className="calendar-wrapper">
            <Calendar
              onDayClick={handleDayClick}
              modifiers={modifiers}
              locale={enUS}
            />
          </div>
          <div className="stat-wrap">
            <div className="stat-block">
              <div className="stat-block__left">
                <p className="stat-num">{successRate}%</p>
              </div>
              <div className="stat-block__rigth">
                <p>
                  SUCCESS
                  <br /> RATE
                </p>
              </div>
            </div>
            <div className="stat-block center">
              <div className="stat-block__left">
                <p className="stat-num">{selectedDateByMonth.length}</p>
              </div>
              <div className="stat-block__rigth">
                <p>
                  GOALS
                  <br />
                  MET
                </p>
              </div>
            </div>
            <div className="stat-block center">
              <div className="stat-block__left">
                <p className="stat-num">{monthStreak}</p>
              </div>
              <div className="stat-block__rigth">
                <p>
                  CURRENT <br />
                  STREAK
                </p>
              </div>
            </div>
            <div className="stat-block end">
              <div className="stat-block__left">
                <p className="stat-num">{bestStreak}</p>
              </div>
              <div className="stat-block__rigth">
                <p>
                  BEST <br />
                  STREAK
                </p>
              </div>
            </div>
          </div>

          <div className="graph-header">
            <h3 className="graph-title">HABIT DEVELOPMENT</h3>
            <div className="switch-wrapper">
              <p>MONTH</p>
              <div className="switch-month-year">
                <label className="switch" htmlFor="checkbox">
                  <input
                    type="checkbox"
                    id="checkbox"
                    checked={checked}
                    onChange={() => setChecked(!checked)}
                  />
                  <span className="slider-switch"></span>
                </label>
              </div>
              <p>YEAR</p>
            </div>
          </div>
          <div className="chart-wrapper">
            {checked ? (
              <ChartYear selectedMonth={selectedMonth} />
            ) : (
              <div>month</div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

_Home.propTypes = {
  selectedMonth: instanceOf(Date),
  updateYearRate: func,
  updateYearStreak: func,
  yearStreakStat: array,
};

const mapStateToProps = (state) => ({
  selectedMonth: state.calendar.selectedMonth,
  yearStreakStat: state.calendar.yearStreakStat,
});

const Home = connect(mapStateToProps, { updateYearRate, updateYearStreak })(
  _Home,
);

export { Home };
