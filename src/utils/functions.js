import { utcToZonedTime } from 'date-fns-tz';
import { TIME_ZONE } from '../constants';

export const getYearMonth = (date) => {
  const monthRate = utcToZonedTime(date, TIME_ZONE).getMonth();
  const yearRate = utcToZonedTime(date, TIME_ZONE).getFullYear();
  return {
    monthRate,
    yearRate,
  };
};

export const namesMonth = (data) => {
  switch (data.month) {
    case 0:
      data.month = 'Jan';
      break;
    case 1:
      data.month = 'Feb';
      break;
    case 2:
      data.month = 'Mar';
      break;
    case 3:
      data.month = 'Apr';
      break;
    case 4:
      data.month = 'May';
      break;
    case 5:
      data.month = 'Jun';
      break;
    case 6:
      data.month = 'Jul';
      break;
    case 7:
      data.month = 'Aug';
      break;
    case 8:
      data.month = 'Sep';
      break;
    case 9:
      data.month = 'Oct';
      break;
    case 10:
      data.month = 'Nov';
      break;
    case 11:
      data.month = 'Dec';
      break;
    default:
      break;
  }
  return data;
};
