import CalendarActionTypes from './calendar.types';

const initialState = {
  selectedMonth: null,
  yearStreakStat: [],
};

const updateYearStreakState = (yearStreakStat, streak) => {
  const existingYearStreakStat = yearStreakStat.find(
    (yearStreak) =>
      yearStreak.month === streak.month && yearStreak.year === streak.year,
  );

  if (existingYearStreakStat) {
    return yearStreakStat.map((yearStreak) =>
      yearStreak.month === streak.month && yearStreak.year === streak.year
        ? (yearStreak = streak)
        : yearStreak,
    );
  } else {
    return (yearStreakStat = [...yearStreakStat, streak]);
  }
};

const calendarReducer = (state = initialState, action) => {
  switch (action.type) {
    case CalendarActionTypes.SET_SELECTED_MONTH:
      return {
        ...state,
        selectedMonth: action.payload,
      };
    case CalendarActionTypes.UPDATE_YEAR_STREAK_STAT:
      return {
        ...state,
        yearStreakStat: updateYearStreakState(
          state.yearStreakStat,
          action.payload,
        ),
      };
    default:
      return state;
  }
};

export default calendarReducer;
