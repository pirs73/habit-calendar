import CalendarActionTypes from './calendar.types';

export const setSelectedMonth = (month) => {
  return (dispatch) => {
    dispatch({
      type: CalendarActionTypes.SET_SELECTED_MONTH,
      payload: month,
    });
  };
};

export const updateYearStreak = (streak) => {
  return (dispatch) => {
    dispatch({
      type: CalendarActionTypes.UPDATE_YEAR_STREAK_STAT,
      payload: streak,
    });
  };
};
