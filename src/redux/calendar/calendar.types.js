const CalendarActionTypes = {
  SET_SELECTED_MONTH: 'SET_SELECTED_MONTH',
  UPDATE_YEAR_STREAK_STAT: 'UPDATE_YEAR_STREAK_STAT',
};

export default CalendarActionTypes;
