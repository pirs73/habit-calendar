import GraphActionTypes from './graph.types';

const initialState = {
  yearRateStat: [],
};

const updateYearRateState = (yearRateStat, rate) => {
  const existingYearRateStat = yearRateStat.find(
    (yearRate) => yearRate.month === rate.month && yearRate.year === rate.year,
  );

  if (existingYearRateStat) {
    return yearRateStat.map((yearRate) =>
      yearRate.month === rate.month && yearRate.year === rate.year
        ? (yearRate = rate)
        : yearRate,
    );
  } else {
    return (yearRateStat = [...yearRateStat, rate]);
  }
};

const graphReducer = (state = initialState, action) => {
  switch (action.type) {
    case GraphActionTypes.UPDATE_YEAR_RATE_STAT:
      return {
        ...state,
        yearRateStat: updateYearRateState(state.yearRateStat, action.payload),
      };
    default:
      return state;
  }
};

export default graphReducer;
