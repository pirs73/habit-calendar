import GraphActionTypes from './graph.types';

export const updateYearRate = (rate) => {
  return (dispatch) => {
    dispatch({
      type: GraphActionTypes.UPDATE_YEAR_RATE_STAT,
      payload: rate,
    });
  };
};
