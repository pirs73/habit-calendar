import { combineReducers } from 'redux';
import calendarReducer from './calendar/calendar.reducer';
import graphReducer from './graph/graph.reducer';

export default combineReducers({
  calendar: calendarReducer,
  graph: graphReducer,
});
