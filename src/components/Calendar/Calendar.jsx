import React, { useEffect } from 'react';
import { bool, func, instanceOf, object, objectOf, string } from 'prop-types';
import { startOfMonth } from 'date-fns';
import { connect } from 'react-redux';
import { isSelectable, mergeModifiers } from '../../utils';
import useControllableState from '../../hooks/useControllableState';
import {
  CalendarNavigation,
  CalendarWeekHeader,
  CalendarGrid,
} from '../../components';
import { setSelectedMonth } from '../../redux/calendar/calendar.actions';

const _Calendar = ({
  locale,
  month: receivedMonth,
  modifiers: receivedModifiers,
  modifiersClassNames,
  minimumDate,
  maximumDate,
  onMonthChange,
  onDayHover,
  onDayClick,
  weekdayFormat,
  touchDragEnabled,
  setSelectedMonth,
}) => {
  const [month, setMonth] = useControllableState(
    receivedMonth,
    onMonthChange,
    startOfMonth(new Date()),
  );

  const modifiers = mergeModifiers(
    { disabled: (date) => !isSelectable(date, { minimumDate, maximumDate }) },
    receivedModifiers,
  );

  useEffect(() => {
    setSelectedMonth(month);
  }, [month, setSelectedMonth]);

  return (
    <>
      <CalendarNavigation
        locale={locale}
        minimumDate={minimumDate}
        maximumDate={maximumDate}
        month={month}
        onMonthChange={setMonth}
      />

      <CalendarWeekHeader locale={locale} weekdayFormat={weekdayFormat} />

      <CalendarGrid
        locale={locale}
        modifiers={modifiers}
        modifiersClassNames={modifiersClassNames}
        month={month}
        onMonthChange={setMonth}
        onDayHover={onDayHover}
        onDayClick={onDayClick}
        touchDragEnabled={touchDragEnabled}
      />
    </>
  );
};

_Calendar.propTypes = {
  locale: object.isRequired,
  minimumDate: instanceOf(Date),
  maximumDate: instanceOf(Date),
  modifiers: objectOf(func),
  modifiersClassNames: objectOf(string),
  month: instanceOf(Date),
  onMonthChange: func,
  onDayHover: func,
  onDayClick: func,
  weekdayFormat: string,
  touchDragEnabled: bool,
  setSelectedMonth: func,
};

const Calendar = connect(null, { setSelectedMonth })(_Calendar);

export { Calendar };
