import React, { useState, useEffect } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid } from 'recharts';
import { connect } from 'react-redux';
import { getYearMonth } from '../../utils';

const _ChartYear = ({ selectedMonth, yearRateStat }) => {
  const initialData = [
    { month: 'Jan', rate: 0, year: 0 },
    { month: 'Feb', rate: 0, year: 0 },
    { month: 'Mar', rate: 0, year: 0 },
    { month: 'Apr', rate: 0, year: 0 },
    { month: 'May', rate: 0, year: 0 },
    { month: 'Jun', rate: 0, year: 0 },
    { month: 'Jul', rate: 0, year: 0 },
    { month: 'Aug', rate: 0, year: 0 },
    { month: 'Sep', rate: 0, year: 0 },
    { month: 'Oct', rate: 0, year: 0 },
    { month: 'Nov', rate: 0, year: 0 },
    { month: 'Dec', rate: 0, year: 0 },
  ];
  const [rateData, setRateData] = useState(initialData);

  useEffect(() => {
    if (selectedMonth) {
      const { yearRate } = getYearMonth(selectedMonth);

      setRateData((prevState) => {
        if (prevState) {
          return prevState.map((prev) => ({
            ...prev,
            year: yearRate,
            rate: 0,
          }));
        }
      });
    }
  }, [selectedMonth]);

  useEffect(() => {
    const statsByYear = (month, stats) => {
      const { yearRate } = getYearMonth(month);
      return stats.filter((stat) => stat.year === yearRate);
    };

    let tmpArr = [];
    tmpArr = [...yearRateStat];

    if (selectedMonth) {
      const data = statsByYear(selectedMonth, tmpArr);
      setRateData((prevState) => {
        if (prevState) {
          return prevState.map((prev) => {
            data.map((el) =>
              prev.month === el.month && prev.year === el.year
                ? (prev.rate = el.rate)
                : prev,
            );
            return prev;
          });
        }
      });
    }
  }, [selectedMonth, yearRateStat]);

  return (
    <LineChart
      width={820}
      height={300}
      data={rateData}
      margin={{
        top: 5,
        right: 0,
        left: 0,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="month" type="category" />
      <YAxis minTickGap={10} tickSize={10} unit="%" domain={[0, 100]} />
      <Line
        type="monotone"
        dataKey="rate"
        stroke="#8884d8"
        activeDot={{ r: 8 }}
      />
    </LineChart>
  );
};

const mapStateToProps = (state) => ({
  yearRateStat: state.graph.yearRateStat,
});

const ChartYear = connect(mapStateToProps)(_ChartYear);
export { ChartYear };
